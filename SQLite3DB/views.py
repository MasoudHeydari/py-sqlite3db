from django.http import HttpResponse

from SQLite3DB.DB.Database import Database

person_table_name = 'Person'
person_table_columns = 'person_id INTEGER PRIMARY KEY AUTOINCREMENT, person_name TEXT, person_family TEXT, person_career TEXT, person_phone_number TEXT'


def do_sql_stuff(request):
    with Database(db_name='masoud.sqlite3') as db:
        db.create_table(person_table_name, person_table_columns)
    # db.insert(person_table_name, columns="person_name, person_family, person_career, person_phone_number",
    #           data="'masoud' , 'Heydari', 'Software Developer', '09195474883'")

    # persons = db.get_all(person_table_name)

    # db.delete(person_table_name, 'person_id = 4')

    # db.update(person_table_name, set_clause="person_name = 'Mohammad'", where_clause='person_id % 2 == 0')

    return HttpResponse("Hello masoud!")
